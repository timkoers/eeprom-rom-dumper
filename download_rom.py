## Author: Arpan Das
## Date:   Fri Jan 11 12:16:59 2019 +0530
## URL: https://github.com/Cyberster/SPI-Based-EEPROM-Reader-Writer

## It listens to serial port and writes contents into a file
## requires pySerial to be installed 
import sys 
import serial
import time
start = time.time()

MEMORY_SIZE = 4096 # In bytes
BITS_PER_SPI_TRANSFER = 8
serial_port = 'COM4'
baud_rate = 115200 # In arduino, Serial.begin(baud_rate)
write_to_file_path = "dump.rom"

output_file = open(write_to_file_path, "wb")
ser = serial.Serial(serial_port, baud_rate)

time.sleep(1) # 1 second

print("Press d for dump ROM else CTRL+C to exit.")
ch = sys.stdin.read(1)

if ch == 'd':    
    print("Dumping EEPROM to dump.rom");
    ser.write('d'.encode('utf-8'))
    for i in range(int(MEMORY_SIZE)):
        sys.stdout.write("\r" + "{0:#0{1}x}".format(i,6) + "-" + "{0:#0{1}x}".format((MEMORY_SIZE - 1),6))
        sys.stdout.flush()
        # wait until arduino response with 'W' i.e. 1 byte of data write request
        while (ser.read() != 'W'.encode('utf-8')): continue
        ser.write('G'.encode('utf-8')) # sends back write request granted signal
        byte = ser.read(1)
        output_file.write(byte)
        #print(byte)
        #print(str(MEMORY_SIZE - (i * 8)) + " bytes remaining.")
        
print('\nIt took', time.time()-start, ' seconds.')
